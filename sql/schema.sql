
CREATE TYPE network_type AS ENUM (
    'FACEBOOK', 
    'TWITTER', 
    'GITHUB',
    'LINKEDIN'
);

CREATE TABLE IF NOT EXISTS users (
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    lastname VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    pass VARCHAR(255) NOT NULL
);

INSERT INTO users (name, lastname, email, pass) VALUES
    ('Eduardo', 'Aguilar Yépez', 'dante.aguilar41@gmail.com', '2066974dceaaa09ca38aa445c200931ea8fb649f92399e8ef3658520dfcfd201');

CREATE TABLE IF NOT EXISTS contacts (
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    lastname VARCHAR(100) NULL,
    email VARCHAR(100) NULL,
    phone VARCHAR(100) NOT NULL,
    address TEXT NULL,
    user_id INTEGER NOT NULL REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS social_networks(
    id SERIAL NOT NULL PRIMARY KEY,
    url VARCHAR(255) NOT NULL,
    network network_type NOT NULL,
    contact_id INTEGER NOT NULL REFERENCES contacts(id) 
);

