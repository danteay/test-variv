<?php 

namespace Libraries;

class DefSchemas
{
    public static function user()
    {
        return (object)[
            "type" => "object",
            "properties" => (object)[
                "name" => (object)[
                    "type" => "string",
                    "minimum" => 1
                ],
                "lastname" => (object)[
                    "type" => "string",
                    "minimum" => 1
                ],
                "email" => (object)[
                    "type" => "email",
                    "minimum" => 5
                ],
                "pass" => (object)[
                    "type" => "string",
                    "minimum" => 8
                ]
            ],
            "required" => ["name", "lastname", "email", "pass"]
        ];
    }
}