<?php

namespace Models;

use Base\BaseModel;

class Contact extends BaseModel
{
    public $id;
    public $name;
    public $lastname;
    public $email;
    public $phone;
    public $address;
    public $user_id;

    /**
     * Contact Construct
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Return all contacts of a specific user id
     * @param int $userId
     * @return array
     * @throws \Exception
     */
    public function findAllByUserId($userId)
    {
        if (!is_int($userId)) {
            throw new \Exception('invalid id value');
        }

        $query = "SELECT * FROM contacts WHERE user_id = " . intval($userId);

        $data = $this->db->query($query);
        $all = [];

        while ($field = $data->fetch()) {
            $obj = new Contact();
            $obj->id = $field['id'];
            $obj->name = $field['name'];
            $obj->lastname = $field['lastname'];
            $obj->email = $field['email'];
            $obj->phone = $field['phone'];
            $obj->address = $field['address'];
            $obj->user_id = $field['user_id'];
            $all[] = $obj;
        }

        return $all;
    }

    /**
     * Insert a new Contact
     * @throws \Exception
     */
    public function save()
    {
        if (!empty($this->id)) {
            $this->update();
            return;
        }

        $query = "INSERT INTO contacts(name, lastname, email, phone, address, user_id) VALUES 
            ('{$this->name}', '{$this->lastname}', '{$this->email}', '{$this->phone}', '{$this->address}', {$this->user_id}) 
            RETURNING id";
        
        $data = $this->db->query($query);
        $data = $data->fetch();

        $this->id = $data['id'];
    }

    /**
     * Update contact information
     */
    public function update()
    {
        $query = "UPDATE contacts SET 
            name = '{$this->name}', 
            lastname = '{$this->lastname}',
            email = '{$this->email}',
            phone = '{$this->phone}',
            address = '$this->address' WHERE id = {$this->id}";

        $this->db->query($query);
    }

    /**
     * Delete a contact of a user
     * @param null|int $id
     * @throws \Exception
     */
    public function delete($id=null)
    {
        if (empty($id)) {
            $id = $this->id;
        } else if (!empty($id) && !is_int($id)) {
            throw new \Exception('invalid id value');
        }

        $query = "DELETE FROM contacts WHERE id = '{$id}'";

        $this->db->query($query);
    }
}