# Slim Scafold

This is a Slim preconfigured project to create a MVC project based on the [slim/Slim-Skeleton](https://github.com/slimphp/Slim-Skeleton) 
repository from [Slim Framework](https://www.slimframework.com/).

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/fe3670dd403c65c7b9d1#?env%5Btest-variv%5D=W3sia2V5IjoibG9jYWwiLCJ2YWx1ZSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCIsImRlc2NyaXB0aW9uIjoiIiwidHlwZSI6InRleHQiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6InVzZXIiLCJ2YWx1ZSI6ImRhbnRlLmFndWlsYXI0MUBnbWFpbC5jb20iLCJkZXNjcmlwdGlvbiI6IiIsInR5cGUiOiJ0ZXh0IiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJwYXNzIiwidmFsdWUiOiIxcWF6MndzeCIsImRlc2NyaXB0aW9uIjoiIiwidHlwZSI6InRleHQiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6InVzZXJfaWQiLCJ2YWx1ZSI6IjEiLCJkZXNjcmlwdGlvbiI6IiIsInR5cGUiOiJ0ZXh0IiwiZW5hYmxlZCI6dHJ1ZX1d)

## Dependendies

* php >= 5.5.0
* slim/slim ^3.1
* slim/php-view ^2.0
* corephp/log ^0.2.1
* doctrine/dbal ^2.7

## Namespaces

| Namespace   | Route folder        |
|-------------|---------------------|
| Controllers | src/app/controllers |
| Helpers     | src/app/helpers     |
| Models      | src/app/models      |
| Middlewares | src/app/middlewares |
| Libraries   | src/app/libraries   |
| Base        | src/app/base        |

## Run the aplication

### Direct execution

```bash
php -S 0.0.0.0:8080 -t public index.php
```

### Composer execution

```bash
composer start
```

### Docker compose

```bash
docker-compose up
```