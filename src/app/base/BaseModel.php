<?php

namespace Base;

class BaseModel
{
    protected $db;
    protected $logger;

    public function __construct()
    {
        global $database;
        global $logger;

        $this->db = $database;
        $this->logger = $logger;
    }
}