<?php

namespace Helpers;

use Models\User;
use Libraries\DefSchemas;
use JsonSchema\Validator as Schema;
use JsonSchema\Constraints\Constraint;

/**
 * User functions
 */
trait UserHelper
{
    /**
     * Return an encrypted password string
     * @param string $pass
     * @return string
     * @throws \Exception
     */
    public function encryptPass($pass)
    {
        if (empty($pass)) {
            throw new \Exception('invalid password string');
        }

        return hash_hmac('sha256', $pass, $_ENV['HASH_KEY']);
    }

    public function validateCreateSchema($body)
    {
        $schema = new Schema();
        $schema->validate($body, DefSchemas::user());

        if (!$schema->isValid()) {
            $message = '';
            foreach ($schema->getErrors() as $error) {
                $message = "[{$error['property']}] {$error['message']} - ";
            }

            throw new \Exception($message);
            
        }
    }
}
