<?php

use Controllers\ApiController;
use Middlewares\AuthMiddleware;
use Middlewares\CorsMiddleware;

$app->get(
    '/users',
    ApiController::class . ':getUser'
);

$app->post(
    '/users',
    ApiController::class . ':createUser'
);

$app->group('/api', function() {
    $this->get(
        '/users/{id}/contacts',
        ApiController::class . ':getContacts'
    );
})
->add(new CorsMiddleware($container))
->add(new AuthMiddleware($container));