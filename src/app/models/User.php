<?php

namespace Models;

use Base\BaseModel;
use Helpers\UserHelper;

class User extends BaseModel
{
    public $id;
    public $name;
    public $lastname;
    public $email;
    public $pass;

    use UserHelper;

    /**
     * User Construct
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Load User data by id
     * @param int $id
     * @throws \Exception
     */
    public function loadById($id)
    {
        if (!is_int($id)) {
            throw new \Exception('invalid id value');
        }

        $query = "SELECT * FROM users WHERE id = " . intval($id);

        $data = $this->db->query($query);
        $data = $data->fetch();

        if (empty($data)) {
            throw new \Exception('not found user');
        }

        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->lastname = $data['lastname'];
        $this->email = $data['email'];
        $this->pass = $data['pass'];
    }

    /**
     * Load User data by email
     * @param string $email
     * @throws \Exception
     */
    public function loadByEmail($email)
    {
        $query = "SELECT * FROM users WHERE email = '{$email}'";

        $data = $this->db->query($query);
        $data = $data->fetch();

        if (empty($data)) {
            throw new \Exception('not found user');
        }

        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->lastname = $data['lastname'];
        $this->email = $data['email'];
        $this->pass = $data['pass'];
    }

    /**
     * Save new User
     * @throws \Exception
     */
    public function save()
    {
        $cryptPass = $this->encryptPass($this->pass);

        $query = "INSERT INTO users(name, lastname, email, pass) VALUES 
            ('{$this->name}', '{$this->lastname}', '{$this->email}', '$cryptPass') RETURNING id";

        $data = $this->db->query($query);
        $data = $data->fetch();

        $this->id = $data['id'];
        $this->pass = $cryptPass;
    }
}