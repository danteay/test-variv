<?php

namespace Models;

use Base\BaseModel;

class SocialNetwork extends BaseModel
{
    public $id;
    public $url;
    public $network;
    public $contact_id;

    /**
     * SocialNetwork Construct
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Return all the networks of a cotact
     * @param int $id
     * @throws \Exception
     */
    public function getAllByCountactId($id)
    {
        if (!is_int($userId)) {
            throw new \Exception('invalid id value');
        }

        $query = "SELECT * FROM social_networks WHERE contact_id = " . intval($id);

        $data = $this->db->query($query);
        $all = [];

        while ($netw = $data->fetch()) {
            $obj = new SocialNetwork();
            $obj->id = $netw['id'];
            $obj->url = $netw['url'];
            $obj->network = $netw['network'];
            $obj->contact_id = $netw['contact_id'];
            $all[] = $obj;
        }

        return $all;
    }

    /**
     * Load network information by id
     * @param int $id
     * @throws \Exception
     */
    public function loadById($id)
    {
        if (!is_int($id)) {
            throw new \Exception('invalid id value');
        }

        $query = "SELECT * FROM social_networks WHERE id = " . intval($id);

        $data = $this->db->query($query);
        $data = $data->fetch();

        $this->id = $data['id'];
        $this->url = $data['url'];
        $this->network = $data['network'];
        $this->contact_id = $data['contact_id'];
    }

    public function save()
    {
        if (!empty($this->id)) {
            $this->update();
            return;
        }

        $query = "INSERT INTO social_networks(url, network, contact_id) VALUES
            ('{$this->url}', '{$this->network}', {$this->contact_id}) RETURNING id";

        $data = $this->db->query($query);
        $data = $data->fetch();

        $this->id = $data['id'];
    }

    public function update()
    {
        $this->db->update(
            'social_networks',
            [
                'url' => $this->url, 
                'network' => $this->network
            ],
            ['id' => $this->id]
        );
    }

    public function delete($id=null)
    {
        if (empty($id)) {
            $id = $this->id;
        } else if (!empty($id) && !is_int($id)) {
            throw new \Exception('invalid id value');
        }

        $this->db->delete(
            'social_networks',
            ['id' => $id]
        );
    }
}