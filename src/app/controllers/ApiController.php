<?php

namespace Controllers;

use Slim\Http\Request;
use Slim\Http\Response;
use Base\BaseController;
use Models\User;
use Models\Contact;
use Models\SocialNetwork;
use Helpers\UserHelper;

class ApiController extends BaseController
{
    use UserHelper;

    /**
     * Constrcut
     * @param mixed $c Application context
     */
    public function __construct($c)
    {
        parent::__construct($c);
    }

    public function getUser(Request $req, Response $res, $args)
    {
        try{
            $contObj = new User();
            $contObj->loadById(intval($args['id']));
        } catch (\Exception $e) {
            $this->logger->erro($e->getMessage());
            return $res->withJson([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        $data = [
            'status' => 'success',
            'message' => 'OK',
            'data' => $contObj
        ];

        return $res->withJson($data, 200);
    }

    public function createUser(Request $req, Response $res, $args)
    {
        try {
            $this->validateCreateSchema(json_decode($req->getBody()));

            $body = $req->getParsedBody();

            if (empty($body)) {
                throw new \Exception("empty request");
            }

            $user = new User();
            $user->name = isset($body['name']) ? $body['name'] : null;
            $user->lastname = isset($body['lastname']) ? $body['lastname'] : null;
            $user->email = isset($body['email']) ? $body['email'] : null;
            $user->pass = isset($body['pass']) ? $body['pass'] : null;

            $user->save();

            return $res->withJson($user, 200);
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->logger->error($message);

            if (preg_match('/SQLSTATE\[23505\]/', $message)) {
                $message = "El correo ya se encuentra registrado, intente con uno diferente.";
            }

            return $res->withJson([
                'status' => 'error',
                'message' => $message
            ], 500);
        }
    }

    public function getContacts(Request $req, Response $res, $args)
    {
        try {
            $contObj = new Contact();
            $all = $contObj->findAllByUserId(intval($args['id']));
        } catch (\Exception $e) {
            $this->logger->erro($e->getMessage());
            return $res->withJson([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }

        $data = [
            'status' => 'success',
            'message' => 'OK',
            'data' => $all
        ];

        return $res->withJson($data, 200);
    }
}