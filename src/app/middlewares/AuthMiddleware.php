<?php

namespace Middlewares;

use Slim\Http\Request;
use Slim\Http\Response;
use Models\User;
use Helpers\UserHelper;

class AuthMiddleware
{
    private $logger;
    private $db;

    use UserHelper;

    /**
     * Construct CorsMiddleware
     * @param mixed $c Application context
     */
    public function __construct($c)
    {
        $this->logger = $c->get('logger');
        $this->db = $c->get('database');
    }

    /**
     * Function that will be call for all incomming request and log the relevant information
     * @param Request $request
     * @param Response $response
     * @param callable $next
     * @return Response
     */
    public function __invoke(Request $req, Response $res, $next)
    {
        $accessDenied = [
            'status' => 'error',
            'message' => 'access denied'
        ];

        if (!$req->hasHeader('Authorization')) {
            $this->logger->error('sin header');
            return $res->withJson($accessDenied, 401);
        }

        $auth = str_replace('Basic ', '', $req->getHeader('Authorization')[0]);

        if (empty($auth)) {
            $this->logger->error('header vacio');
            return $res->withJson($accessDenied, 401);
        }

        $auth = explode(':', base64_decode($auth));

        if (sizeof($auth) < 2) {
            $this->logger->error('auth incompleto');
            return $res->withJson($accessDenied, 401);
        }

        try {
            $user = new User();
            $user->loadByEmail($auth[0]);

            $hash = $this->encryptPass($auth[1]);

            if ($user->pass != $hash) {
                $this->logger->error("pass diferente {$hash} - {$user->pass}");
                return $res->withJson($accessDenied, 401);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return $res->withJson($accessDenied, 401);
        }

        return $next($req, $res);
    }
}